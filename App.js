import React from 'react';
import ScreenExchangePointsList from "./Components/ScreenExchangePointsList";
import {createDrawerNavigator, createStackNavigator} from "react-navigation";
import ScreenExchangePointsSingle from "./Components/ScreenExchangePointsSingle";
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducer from "./reducer";
import {ScreenDrawerLeftMenu} from "./Components/ScreenDrawerLeftMenu";

let NormalStack = createStackNavigator({
    ExchangePointsList: { screen: ScreenExchangePointsList },
    ExchangePointsSingle: { screen: ScreenExchangePointsSingle },
}, {
    initialRouteName: 'ExchangePointsList',
    headerMode: 'none',
});

const RootStack = createDrawerNavigator({
    NormalStack: NormalStack
}, {
    initialRouteName: 'NormalStack',
    headerMode: 'none',
    drawerWidth: 250,
    contentComponent: ScreenDrawerLeftMenu, // компонент, который будет показан в качестве выдвижного левого меню
});


const store = createStore(reducer);

export default class App extends React.Component {
    render() {
        return <Provider store={store}>
            <RootStack />
        </Provider>;
    }
}



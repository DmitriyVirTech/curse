import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    BodyBackground:{
        position: 'absolute',
        width: '100%',
        height: '100%',
        margin: 0,
        padding: 0,
        zIndex: 0,
    },
    AppTitle:{
        display: 'flex',
        paddingLeft: 20,
        paddingTop: 25,
        paddingRight: 20,
        width: '100%',
        height: 50 ,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    AppMenu:{
        backgroundColor: '#357bd0',
        width: 50 ,
        height: 50 ,
        borderBottomLeftRadius: 7,
        borderBottomRightRadius: 7,
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    AppMenuImg:{
        width: '80%',
        height: '80%',
    },
    AppTitleText:{
        color: '#fff',
        fontSize: 13,
        paddingLeft: 10,
    },
    AppBody:{
        padding: 20,
        paddingTop: 0,
        width: '100%',
    },
    CurseItem:{
        marginTop: 20,
        width: '100%',
        height: 70,
        padding: 10,
        backgroundColor: 'rgba(53,123,208,0.7)',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    CurseItemIcon:{
        height: '90%',
        width: '13%'
    },
    CurseItemAbout:{
        width: '23%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    CurseItemAboutName:{
        color: '#fff',
        fontSize: 12,
    },
    CurseItemAboutDate:{
        color: '#fff',
        fontSize: 8,
    },
    CurseItemValuteCont: {
        width: '58%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    CurseItemValuteVal: {
        fontSize: 9,
        color: '#fff',
        textAlign: 'center',
    },
    CurseItemValuteImg:{
        width: 35,
        height: '45%',
    },
    SingleContainer:{
        padding: 15,
        backgroundColor: 'rgba(18, 97, 185, 0.76)',
        borderRadius: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    SingleTitle:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    BackImg:{
        width: 40,
        height: 40,
    },
    SingleTitleLeftHead:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    SingleTitleLeftHeadText:{
        color: '#fff',
        fontSize: 22,
        paddingTop: 10,
        marginLeft: 15,
    },
    SingleIcon: {
        width: 100,
        height: 100,
    },
    SingleTitleLeftText:{
        color: '#fff',
        fontSize: 18,
        marginTop: 15,
        textAlign: 'left',
    },
    SinglValute:{
        marginTop: 40,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    SinglValuteItem:{
        width: '25%',
        display: 'flex',
        alignItems: 'center',
    },
    SingleValuteItemImg:{
        width: 80,
        height: 50,
    },
    SingleValuteItemText:{
        fontSize: 14,
        color: '#fff',
        textAlign: 'center',
        marginTop: 10,
    },
    SingleDescription:{
        marginTop: 25,
        paddingLeft: 20,
        paddingRight: 20,
    },
    SingleDescriptionText:{
        color: '#fff',
        fontSize: 13,
    },

});

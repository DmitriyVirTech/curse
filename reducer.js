export const GET_ALL_EXCHANGE_POINTS_STARTED = 'GET_ALL_EXCHANGE_POINTS_STARTED';
export const GET_ALL_EXCHANGE_POINTS_SUCCESS = 'GET_ALL_EXCHANGE_POINTS_SUCCESS';

const initialState = {
    exchangePointsList: []
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_EXCHANGE_POINTS_STARTED:
            return { ...state, loading: true };
        case GET_ALL_EXCHANGE_POINTS_SUCCESS:
            console.log('получен список обменников');
            return { ...state, loading: false, exchangePointsList: action.exchangePointsList };
        default:
            return state;
    }
}

export function getAllExchangePoints(next) {
    fetch('http://kurs.virtech.kz/exchangePoints.php')
        .then((response) => response.json())
        .then((responseJson) => {
            next(responseJson);
        })
        .catch((error) => {
            console.error(error);
        });
    return {
        type: GET_ALL_EXCHANGE_POINTS_STARTED,
    };
}

export function allExchangePointsReceived(data) {
    return {
        type: GET_ALL_EXCHANGE_POINTS_SUCCESS,
        exchangePointsList: data,
    };
}

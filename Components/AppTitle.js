//ЗАГОЛОВОК
import React from "react";
import AppMenuButton from "./AppMenuButton";
import {styles} from "../Styles";
import {Text, View} from "react-native";


export default class AppTitle extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.AppTitle}>
                <AppMenuButton navigation={this.props.navigation}/>
                <View>
                    <Text style={styles.AppTitleText}>Курсы валют в Петропавловске</Text>
                </View>
            </View>
        );
    }
}
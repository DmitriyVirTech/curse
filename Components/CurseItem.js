import React from "react";
import {styles} from "../Styles";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {navigate} from "react-navigation";

export default class CurseItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity key={this.props.data.id} onPress={() => this.props.navigation.navigate('ExchangePointsSingle', {data: this.props.data})}>
                <View style={styles.CurseItem}>
                    <Image style={styles.CurseItemIcon} source={require('../sber.png')} />
                    <View style={styles.CurseItemAbout}>
                        <Text style={styles.CurseItemAboutName}>{this.props.data.name}</Text>
                        <Text style={styles.CurseItemAboutDate}>Обновлен: {this.props.data.refreshDateTime}</Text>
                    </View>
                    <View style={styles.CurseItemValuteCont}>
                        <View style={styles.CurseItemValute}>
                            <Image style={styles.CurseItemValuteImg} source={require('../eur.png')} />
                            <Text style={styles.CurseItemValuteVal}>{this.props.data.EUR.sell}</Text>
                            <Text style={styles.CurseItemValuteVal}>{this.props.data.EUR.buy}</Text>
                        </View>
                        <View style={styles.CurseItemValute}>
                            <Image style={styles.CurseItemValuteImg} source={require('../usd.png')} />
                            <Text style={styles.CurseItemValuteVal}>409.20</Text>
                            <Text style={styles.CurseItemValuteVal}>412.70</Text>
                        </View>
                        <View style={styles.CurseItemValute}>
                            <Image style={styles.CurseItemValuteImg} source={require('../rub.png')} />
                            <Text style={styles.CurseItemValuteVal}>409.20</Text>
                            <Text style={styles.CurseItemValuteVal}>412.70</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

}
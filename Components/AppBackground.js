import React from 'react';
import {styles} from "../Styles";
import {ImageBackground} from "react-native";

let picture = { uri: 'http://desktopwallpapers.org.ua/pic/201202/640x480/desktopwallpapers.org.ua-12370.jpg' };

//ФОН
export default class AppBackground extends React.Component {
    render() {
        return (
            <ImageBackground source={ picture } style={styles.BodyBackground} />
        );
    }
}
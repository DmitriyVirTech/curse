//КНОПКА МЕНЮ
import React from "react";
import {styles} from "../Styles";
import {Image, Text, TouchableOpacity, View} from "react-native";

export default class AppMenuButton extends React.Component {

    constructor(props) {
        super(props);
    }

    onMenuButtonPress() {
        this.props.navigation.openDrawer();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.onMenuButtonPress.bind(this)}>
                <View style={styles.AppMenu}>
                    <Image style={styles.AppMenuImg} source={require('../menu.png')} />
                </View>
            </TouchableOpacity>
        );
    }
}

import React from "react";
import {styles} from "../Styles";
import {ScrollView, StatusBar, View, RefreshControl} from "react-native";
import CurseItem from "./CurseItem";
import AppBackground from "./AppBackground";
import AppTitle from "./AppTitle";
import {connect} from "react-redux";
import {allExchangePointsReceived, getAllExchangePoints} from "../reducer";

class ScreenExchangePointsList extends React.Component {

    constructor(props) {
        super(props);
    }
    onRefresh = () => {
        this.props.getAllExchangePoints(this.props.allExchangePointsReceived);
    };

    componentDidMount = () => {
        this.props.getAllExchangePoints(this.props.allExchangePointsReceived);
    };
    
    render() {
        let navigation = this.props.navigation; // анонимная функция внутри map не знает о this,
        // поэтому нам нужно использовать внутри нее отдельно созданную переменную

        if (this.props.exchangePointsList.length === 0) {
            return null;
        }

        return (

            <View style={styles.container}>
                <StatusBar hidden={true} />
                <AppBackground/>
                <AppTitle navigation={this.props.navigation}/>
                <ScrollView style={{width: '100%', height: 50, flexDirection: 'column', marginTop: 30}} refreshControl={
                    <RefreshControl
                        refreshing={this.state}
                        onRefresh={this.onRefresh}
                    />
                }>
                
                    <View style={styles.AppBody}>
                        {this.props.exchangePointsList.map(function(exchangePointData){
                            return(<CurseItem navigation={navigation} data={exchangePointData} key={exchangePointData.id} />);
                        })}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        exchangePointsList: state.exchangePointsList
    };
};

const mapDispatchToProps = {
    getAllExchangePoints,
    allExchangePointsReceived
};

export default connect(mapStateToProps, mapDispatchToProps)(ScreenExchangePointsList);
import React from "react";
import {styles} from "../Styles";
import {ScrollView, StatusBar, Text, TouchableOpacity, View, Image, RefreshControl} from "react-native";
import AppBackground from "./AppBackground";
import {connect} from "react-redux";
import {allExchangePointsReceived, getAllExchangePoints} from "../reducer";

export class ScreenDrawerLeftMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }


    render() {
        console.log('left menu');
        return (
            <View style={styles.container}>
                <Text>our drawer</Text>
            </View>
        );
    }
}


const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ScreenDrawerLeftMenu);
//КОНТЕЙНЕР ПРИЛОЖЕНИЯ
import React from "react";
import {styles} from "../Styles";
import {ScrollView, StatusBar, Text, TouchableOpacity, View, Image, RefreshControl} from "react-native";
import AppBackground from "./AppBackground";
import {connect} from "react-redux";
import Communications from 'react-native-communications';
import {allExchangePointsReceived, getAllExchangePoints} from "../reducer";

export class ScreenExchangePointsSingle extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: this.props.navigation.getParam('data', {}),
            refreshing: false,
            id: this.props.navigation.getParam('data', {}).id,
        };
    }

    onRefresh = () => {
        this.props.getAllExchangePoints(this.props.allExchangePointsReceived);
    };

    componentWillReceiveProps = (nextProps) => {
        var that = this;
        nextProps.exchangePointsList.forEach(function(singleExchangePoint) {
            if (singleExchangePoint.id === that.state.id) {
                that.setState({
                    data: singleExchangePoint
                });
            }
        });
        console.log('componentWillReceiveProps');
    };

    render() {
 
        return (
            <View style={styles.container}>
                <StatusBar hidden={true} />
            
                <AppBackground/>
            
                <ScrollView style={{width: '100%', height: 50, flexDirection: 'column', marginTop: 30}} refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }>
            
                <View style={styles.SingleContainer}>
                    <View style={styles.SingleTitle}>
            
                        <View style={styles.SingleTitleLeft}>
            
                            <View style={styles.SingleTitleLeftHead}>
            
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('ExchangePointsList', {})}>
                                    <Image style={styles.BackImg} source={require('../back.png')} />
                                </TouchableOpacity>
                                                                          
                                <Text style={styles.SingleTitleLeftHeadText}>{this.state.data.name}</Text>
                                <Text style={styles.SingleTitleLeftHeadText} onPress={() => Communications.phonecall('+77082487423', true)}>8 777 777 77 77</Text>
                            </View> 
                            
                            <Text style={styles.SingleTitleLeftText}>{this.state.data.phone}</Text>
                            <Text style={styles.SingleTitleLeftText}>{this.state.data.mail}</Text>
                            <Text style={styles.SingleTitleLeftText}>{this.state.data.adr}</Text>
                                                                          
                        </View>
                                                                          
                        <View style={styles.SingleTitleRight}>
                            <Image style={styles.SingleIcon} source={require('../sber.png')} />
                        </View>  

                    </View>
                                                                     
                    <View style={styles.SinglValute}>
                        <View style={styles.SingleValuteItem}>
                            <Image style={styles.SingleValuteItemImg} source={require('../eur.png')} />
                            <Text style={styles.SingleValuteItemText}>Покупка: {this.state.data.EUR.buy}</Text>
                            <Text style={styles.SingleValuteItemText}>Продажа: {this.state.data.EUR.sell}</Text>
                        </View>

                        <View style={styles.SingleValuteItem}>
                            <Image style={styles.SingleValuteItemImg} source={require('../usd.png')} />
                            <Text style={styles.SingleValuteItemText}>Покупка: {this.state.data.USD.buy}</Text>
                            <Text style={styles.SingleValuteItemText}>Продажа: {this.state.data.USD.sell}</Text>
                        </View>

                        <View style={styles.SingleValuteItem}>
                            <Image style={styles.SingleValuteItemImg} source={require('../rub.png')} />
                            <Text style={styles.SingleValuteItemText}>Покупка: {this.state.data.RUB.buy}</Text>
                            <Text style={styles.SingleValuteItemText}>Продажа: {this.state.data.RUB.buy}</Text>
                        </View>
                    </View>

                    <View style={styles.SingleDescription}>
                           <Text style={styles.SingleDescriptionText}>{this.state.data.description}</Text>
                    </View>
                </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        refreshing: state.loading,
        exchangePointsList: state.exchangePointsList,
    };
};

const mapDispatchToProps = {
    getAllExchangePoints,
    allExchangePointsReceived
};

export default connect(mapStateToProps, mapDispatchToProps)(ScreenExchangePointsSingle);